Elasticsearch::Model.client = Elasticsearch::Client.new url: ENV['ELASTICSEARCH_URL'] || "http://localhost:9200/"

Movie.send :include, ElasticsearchSearchable

unless Movie.__elasticsearch__.index_exists?
  Movie.__elasticsearch__.create_index! force: true
  Movie.import
end
