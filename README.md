= Full stack developer code challenge

This code challenge should be something simple, just to see how you code.
It shouldn't take much of your time, our suggestion is to just implement it in the way you are used to.

== Instructions to run it

=== The app dependencies are:
* Mongodb
* elasticsearch

=== Url to check the deployed app
* http://spree-movie.herokuapp.com/movies

== Just to give you some guidance, check our current stack:

* DynamoDB (production), DynamoDBLocal (development)
* Amazon SQS
* Amazon Elasticsearch
* AWS Lambda (Node.js)
* Docker
* Ruby
* Rails
* React
* ERB
* CoffeeScript (but we are also experimenting ECMAScript 6)
* Sass

But you are free to use whatever services, libraries you'd like.

== Movie catalogue

Functional requirements:

* Users can create and edit movies with the following fields: title, storyline, release date, genres and IMDb link.
* Users can delete movies.
* Users cannot insert movies without a title.
* Non-functional requirements:

The project should have all instructions to run it locally.
The implementation should be in Ruby.

=== Desirable:

* Use elements of our current stack if you are familiar with them.
* Deploy the application, so we can access it.
* Users can search movies by title.
* Attention to detail for the end user experience.
