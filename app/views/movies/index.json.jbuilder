json.array!(@movies) do |movie|
  json.extract! movie, :id, :title, :storyline, :release, :imdb
  json.url movie_url(movie, format: :json)
end
