module ApplicationHelper
  def genres_format(genres)
    genres.pluck(:name).join(' ')
  end
end
