class Movie
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  embeds_many :genres
  accepts_nested_attributes_for :genres, reject_if: :all_blank, autosave: true, autobuild: true

  field :title, type: String
  field :storyline, type: String
  field :release, type: Date
  field :imdb, type: String

  validates :title, presence: true
end
