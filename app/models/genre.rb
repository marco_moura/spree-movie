class Genre
  include Mongoid::Document
  embedded_in :movie, inverse_of: :genres

  field :name, type: String

  validates :name, presence: true
end
