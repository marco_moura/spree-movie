require 'elasticsearch/model'

module ElasticsearchSearchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks

    def as_indexed_json(options={})
      as_json(except: [:id, :_id])
    end
  end
end
