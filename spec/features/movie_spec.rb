require 'rails_helper'

RSpec.feature 'Movie' do
  scenario 'User create a add new movie' do
    visit '/'
    expect(page.body).to have_content 'Listing'
    click_link 'New'
    click_button 'Create Movie'
    expect(page.body).to have_content '1 error prohibited this movie from being saved: ' \
                                      'Title can\'t be blank'
    fill_in 'Title', with: 'Star Wars'
    click_button 'Create Movie'
    expect(page.body).to have_content 'success'
  end

  scenario 'User searchs a movie', :elasticsearch do
    Movie.create! title: 'The Dark Knight'
    sleep 1
    visit '/'
    fill_in 'Search', with: 'dark'
    click_button 'Go'

    expect(page.body).to have_content 'Dark'
  end

  scenario 'User edit a movie' do
    Movie.create! title: 'The Dark Knight'
    visit '/'
    click_on 'Edit'

    fill_in 'Release', with: '2015-10-11'
    click_button 'Update Movie'

    expect(page.body).to have_content 'Movie was successfully updated'
    expect(page.body).to have_content 'Release: 2015-10-11'
  end

  scenario 'User choose a gender', js: true do
    visit '/'
    click_link 'New'
    fill_in 'Title', with: 'Star Wars'
    click_link 'Add Genre'

    expect(page.body).to have_link 'remove genre'

    fill_in 'genre', with: 'fiction'

    click_button 'Create Movie'

    expect(page.body).to have_content 'Movie was successfully created'
    expect(page.body).to have_content 'Genres: fiction'
  end
end
