require 'rails_helper'

RSpec.describe "movies/index" do
  before(:each) do
    assign(:movies, [
      Movie.create!(
        :title => "Title",
        :storyline => "MyText",
        :imdb => "Imdb"
      ),
      Movie.create!(
        :title => "Title",
        :storyline => "MyText",
        :imdb => "Imdb"
      )
    ])
  end

  it "renders a list of movies" do
    render
    assert_select "tr>td", :text => "Title", :count => 2
    assert_select "tr>td", :text => "MyText", :count => 2
    assert_select "tr>td", :text => "Imdb", :count => 2
  end
end
