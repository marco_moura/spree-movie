require 'rails_helper'

RSpec.describe "movies/edit" do
  before(:each) do
    @movie = assign(:movie, Movie.create!(
      :title => "MyString",
      :storyline => "MyText",
      :imdb => "MyString"
    ))
  end

  it "renders the edit movie form" do
    render

    assert_select "form[action=?][method=?]", movie_path(@movie), "post" do

      assert_select "input#movie_title[name=?]", "movie[title]"

      assert_select "textarea#movie_storyline[name=?]", "movie[storyline]"

      assert_select "input#movie_imdb[name=?]", "movie[imdb]"
    end
  end
end
