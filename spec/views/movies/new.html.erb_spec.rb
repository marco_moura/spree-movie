require 'rails_helper'

RSpec.describe "movies/new" do
  before(:each) do
    assign(:movie, Movie.new(
      :title => "MyString",
      :storyline => "MyText",
      :imdb => "MyString"
    ))
  end

  it "renders new movie form" do
    render

    assert_select "form[action=?][method=?]", movies_path, "post" do

      assert_select "input#movie_title[name=?]", "movie[title]"

      assert_select "textarea#movie_storyline[name=?]", "movie[storyline]"

      assert_select "input#movie_imdb[name=?]", "movie[imdb]"
    end
  end
end
