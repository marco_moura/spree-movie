require 'rails_helper'

RSpec.describe "movies/_form" do
  before do
    assign :movie, Movie.new(title: "MyString")
    render partial: 'form'
  end

  it "renders new movie form" do
    assert_select "form[action=?][method=?]", movies_path, "post" do
      assert_select "input#movie_title[name=?]", "movie[title]"
      assert_select "textarea#movie_storyline[name=?]", "movie[storyline]"
      assert_select "input#movie_imdb[name=?]", "movie[imdb]"
      assert_select "input#movie_release[name=?]", "movie[release]"
    end
  end
end
