require 'rails_helper'

RSpec.describe Movie do
  it { is_expected.to embed_many :genres }
  it { is_expected.to have_fields :title, :storyline, :release, :imdb }
  it { is_expected.to validate_presence_of :title }
  it { is_expected.to accept_nested_attributes_for :genres }
end
