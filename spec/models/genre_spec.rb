require 'rails_helper'

RSpec.describe Genre do
  it { is_expected.to have_fields :name }
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to be_embedded_in :movie }
end
